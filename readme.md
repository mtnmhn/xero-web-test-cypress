# web-tests-cypress
This project is a hybrid end to end and component test Automation framework for the Frontend written in Javascript using Cypress and docker.

## Features

* Can be plugeed into any CI tools.
* Can run Cypress tests locally and also using docker-compose. This can be extended to create component integration tests.
* This framework supports testing on multiple browsers. Have included support for Chrome and Edge.
* Screenshots will automatically be available in case of failure.
* Html reports and Videos are available.


## Task Given

As a Xero User,
In order to manage my business successfully,
I want to be able to add an “ANZ (NZ)” bank account inside any Xero Organisation.

## Soulution:

I have choosen to use **[Cypress](https://www.cypress.io/)** to write these tests. 

### Why Cypress 

* Cypress is a next generation front end testing tool built for the modern web.
* Cypress makes it simple to , 
    - Set up tests
    - Write tests
    - Run tests &
    - Debug tests

Cypress is most often compared to Selenium; however Cypress is both fundamentally and architecturally different. Cypress is not constrained by the same restrictions as Selenium.

> [more info here](https://docs.cypress.io/guides/overview/why-cypress.html#In-a-nutshell)

## How to Run the tests in this repository
This project can be run using two different methods listed below
- using docker
- using cypress locally
Feel free to use the one you prefer.

### Pre-requisites & common steps for both methods
    Follow below link and set up git.
* [How to set up Git](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git)

 Once done follow the below steps in order using a Terminal or Command prompt.

1. **`cd`** - navigate to a path you want to clone this framework e.g.`" Windows: cd C:\ "` or `"MAC: 'cd /Users/{username}/'"`
2. **`mkdir automation`**  -  create a new directory with name automation
3. **`cd automation`** - navigate to the folder named automation 
4. **`git init`** - initialise empty git repo
5. **`git clone https://mtnmhn@bitbucket.org/mtnmhn/xero-web-test-cypress.git`** - Clone this repository.

## Run cypress locally

*Pre-requisites*

* [How to Install and Use Node.js and npm (Mac, Windows, Linux)](https://www.taniarascia.com/how-to-install-and-use-node-js-and-npm-mac-and-windows/)
>Versions I have used `node:v10.15.3` and `npm:6.4.1`. 


1. **`cd xero-web-test-cypress/e2e`** 
2. **`npm install`** - get all locally required dependencies under node_modules 
 > This step might take a while only during first time setup.
3. **`./cy-run-prod-chrome.sh`** to run all the tests in `chrome` You can also run tests in `edge` using `./cy-run-prod-edge.sh`. You will see the tests running in chrome and the logs appearing in the console. 
4. Screenshots will be automatically appearing under the `cypress/screenshot` directory if something goes wrong.
5. Cypress can also produce vedios which will be available under 'cypress/videos' directory.
6. Html reports will be generated in the folder `cypress/report/mocha`.

## Run using Docker

*Pre-requisites*

* [How to install Docker](https://docs.docker.com/docker-for-windows/install/)

1. **`cd xero-web-test-cypress`** 
2. `./run-docker-compose-prod.sh` and enter.
 > This step might take a while only during first time setup.
3. You will see the test log on the console.
4. Screenshots will be automatically appearing under the `cypress/screenshot` directory if something goes wrong.
5. Cypress can also produce vedios which will be available under 'cypress/videos' directory.
6. Html reports will be generated in the folder `cypress/report/mocha`.


*Note:*

 * If you want to open cypress to manually debug stuff ,from the home directory use `./cy-open-local.sh`
  
*Troubleshooting:*

* use `chmod +x filename` if you get permission issues when running `.sh files`