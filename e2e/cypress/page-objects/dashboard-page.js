/// <reference types="cypress" />

export function navigate() {
    cy.get('[data-navigation-id="dashboard"]').click()
    cy.contains('Welcome to Xero')
}

export function checkBankAccountDetailsAreCorrect(accountName, accountNumber) {
    cy.get('header>a>h3').contains(accountName).should('contain.text', accountName)
    cy.get('header>a>div').contains(accountNumber).should('contain.text', accountNumber)
}