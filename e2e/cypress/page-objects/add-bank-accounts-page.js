/// <reference types="cypress" />

const pageTitle = 'Add Bank Accounts' 

export function searchBank(bankName) {

    cy.get('#xui-searchfield-1018-inputEl').type(bankName)
}

export function selectBank(bankName) {
    cy.get('#dataview-1025 > .ba-banklist--item').contains(bankName).click()
}

export function enterAccountName(accountName) {
    cy.get('#accountname-1037-inputEl').type(accountName)
}

export function validateAccountNameRequired() {
    cy.get('#accountname-1037-errorEl').should('contain.text', 'Account Name required')
    cy.contains(pageTitle)
}

export function selectAccounType() {
    cy.get('#accounttype-1039-inputEl').click()
    cy.contains('Everyday (day-to-day)').click()
}

export function validateAccountTypeRequired() {
    cy.get('#accounttype-1039-errorEl').should('contain.text', 'Account Type required')
    cy.contains(pageTitle)
}

export function enterAccountNumber(accountnumber) {
    cy.get('#accountnumber-1068-inputEl').type(accountnumber)
}

export function validateAccountNumberRequired() {
    cy.get('#accountnumber-1068-errorEl').should('contain.text', 'Account Number required')
    cy.contains(pageTitle)
}

export function clickContinue() {
    cy.get('#common-button-submit-1015-btnInnerEl').click()
}
