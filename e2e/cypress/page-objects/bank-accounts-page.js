/// <reference types="cypress" />

export function navigate() {
    cy.get(':nth-child(3) > .xrh-focusable').click()
    cy.contains('Bank accounts').click()
}

export function addBankAccount() {
    cy.get('.document > :nth-child(2) > a > .text').click()
}
