/// <reference types="cypress" />

import * as homePage from '../page-objects/login-page.js'
import * as dashboardPage from '../page-objects/dashboard-page.js'
import * as bankAccountsPage from '../page-objects/bank-accounts-page.js'
import * as addBankAccountsPage from '../page-objects/add-bank-accounts-page.js'

describe('Bank Account Tests', () => {
    beforeEach(() => {
        homePage.visitXero()
        homePage.login()
        bankAccountsPage.navigate()
        bankAccountsPage.addBankAccount()

    })

    it('Can add ANZ bank account to an existing organisation', () => {
        //Arrange
        const uniqueNumber = Date.now()
        const bankName = 'ANZ (NZ)'
        const accountName = 'ANZ_' + uniqueNumber
        const accountnumber = uniqueNumber

        //Act
        addBankAccountsPage.searchBank(bankName)
        addBankAccountsPage.selectBank(bankName)
        addBankAccountsPage.enterAccountName(accountName)
        addBankAccountsPage.selectAccounType()
        addBankAccountsPage.enterAccountNumber(accountnumber)
        addBankAccountsPage.clickContinue()
        dashboardPage.navigate()

        //Assert
        dashboardPage.checkBankAccountDetailsAreCorrect(accountName, accountnumber)
    })

    it('Can not add bank account with invalid/blank data to an existing organisation', () => {
        //Arrange
        const bankName = 'ANZ (NZ)'

        //Act & Assert
        addBankAccountsPage.searchBank(bankName)
        addBankAccountsPage.selectBank(bankName)

        addBankAccountsPage.clickContinue()
        addBankAccountsPage.validateAccountNameRequired()
        addBankAccountsPage.enterAccountName('valid name')
        addBankAccountsPage.clickContinue()

        addBankAccountsPage.validateAccountTypeRequired()
        addBankAccountsPage.selectAccounType()
        addBankAccountsPage.clickContinue()

        addBankAccountsPage.validateAccountNumberRequired()

    })
})